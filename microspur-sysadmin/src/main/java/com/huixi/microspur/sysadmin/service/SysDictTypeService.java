package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.entity.SysDictType;

/**
 * <p>
 * 字典主表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysDictTypeService extends IService<SysDictType> {

}

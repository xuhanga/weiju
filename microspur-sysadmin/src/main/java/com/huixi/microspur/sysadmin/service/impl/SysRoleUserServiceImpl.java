package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.entity.SysRoleUser;
import com.huixi.microspur.sysadmin.mapper.SysRoleUserMapper;
import com.huixi.microspur.sysadmin.service.SysRoleUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements SysRoleUserService {

}

package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.entity.SysDictType;
import com.huixi.microspur.sysadmin.mapper.SysDictTypeMapper;
import com.huixi.microspur.sysadmin.service.SysDictTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 字典主表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements SysDictTypeService {

}

package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.WjUserWx;
import com.huixi.microspur.web.mapper.WjUserWxMapper;
import com.huixi.microspur.web.service.WjUserWxService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjUserWxServiceImpl extends ServiceImpl<WjUserWxMapper, WjUserWx> implements WjUserWxService {

}

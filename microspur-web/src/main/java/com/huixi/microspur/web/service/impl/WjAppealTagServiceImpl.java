package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.WjAppealTag;
import com.huixi.microspur.web.mapper.WjAppealTagMapper;
import com.huixi.microspur.web.service.WjAppealTagService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求-对应标签 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealTagServiceImpl extends ServiceImpl<WjAppealTagMapper, WjAppealTag> implements WjAppealTagService {

}

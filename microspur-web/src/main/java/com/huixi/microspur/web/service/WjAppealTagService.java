package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.WjAppealTag;

/**
 * <p>
 * 诉求-对应标签 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealTagService extends IService<WjAppealTag> {

}

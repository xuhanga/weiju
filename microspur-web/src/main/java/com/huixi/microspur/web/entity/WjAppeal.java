package com.huixi.microspur.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 诉求表
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal")
@ApiModel(value="WjAppeal对象", description="诉求表")
public class WjAppeal implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "诉求的id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "诉求对应的用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "诉求的标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "诉求的内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "诉求点赞量")
    @TableField("endorse_count")
    private Integer endorseCount;

    @ApiModelProperty(value = "诉求的评论量")
    @TableField("comment_count")
    private Integer commentCount;

    @ApiModelProperty(value = "诉求浏览量")
    @TableField("browse_count")
    private Integer browseCount;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人(可以是名称 或者 uuid)")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改时间")
    @TableField("update_date")
    private LocalDateTime updateDate;

    @ApiModelProperty(value = "修改人(可以是名称 或者 uuid)")
    @TableField("update_by")
    private String updateBy;

    @ApiModelProperty(value = "状态码")
    @TableField("status")
    private Integer status;


}

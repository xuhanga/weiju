package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.WjChatRecord;
import com.huixi.microspur.web.mapper.WjChatRecordMapper;
import com.huixi.microspur.web.service.WjChatRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室-聊天记录 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjChatRecordServiceImpl extends ServiceImpl<WjChatRecordMapper, WjChatRecord> implements WjChatRecordService {

}

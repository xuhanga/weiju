package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.WjAppealMaterial;
import com.huixi.microspur.web.mapper.WjAppealMaterialMapper;
import com.huixi.microspur.web.service.WjAppealMaterialService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealMaterialServiceImpl extends ServiceImpl<WjAppealMaterialMapper, WjAppealMaterial> implements WjAppealMaterialService {

}

package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.WjAppealMaterial;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealMaterialService extends IService<WjAppealMaterial> {

}

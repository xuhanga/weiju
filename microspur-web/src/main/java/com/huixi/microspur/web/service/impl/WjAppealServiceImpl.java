package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.WjAppeal;
import com.huixi.microspur.web.mapper.WjAppealMapper;
import com.huixi.microspur.web.service.WjAppealService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealServiceImpl extends ServiceImpl<WjAppealMapper, WjAppeal> implements WjAppealService {

}

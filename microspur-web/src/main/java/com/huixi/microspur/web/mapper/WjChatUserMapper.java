package com.huixi.microspur.web.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.entity.WjChatUser;

/**
 * <p>
 * 聊天室对应的用户 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatUserMapper extends BaseMapper<WjChatUser> {

}

package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.WjChat;

/**
 * <p>
 * 聊天室（聊天列表） 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatService extends IService<WjChat> {

}

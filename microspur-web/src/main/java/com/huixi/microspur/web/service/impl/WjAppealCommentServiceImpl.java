package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.entity.WjAppealComment;
import com.huixi.microspur.web.mapper.WjAppealCommentMapper;
import com.huixi.microspur.web.service.WjAppealCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求-评论 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealCommentServiceImpl extends ServiceImpl<WjAppealCommentMapper, WjAppealComment> implements WjAppealCommentService {

}

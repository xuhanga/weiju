package com.huixi.microspur.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 动态评论表
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_dynamic_comment")
@ApiModel(value="WjDynamicComment对象", description="动态评论表")
public class WjDynamicComment implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "动态评论id")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "是否是评价评论的评论（有值代表评价了评论，反之为空就不是）")
    @TableField("about_comment")
    private String aboutComment;

    @ApiModelProperty(value = "对应的动态id")
    @TableField("dynamic_id")
    private String dynamicId;

    @ApiModelProperty(value = "发起评论的用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "评论的内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "评论的类型（1：文字 2：图片）")
    @TableField("content_type")
    private String contentType;

    @ApiModelProperty(value = "素材的大小(KB)")
    @TableField("file_size")
    private String fileSize;

    @ApiModelProperty(value = "评论的楼层")
    @TableField("floor")
    private Integer floor;

    @ApiModelProperty(value = "评论的点赞数")
    @TableField("endorse_count")
    private Integer endorseCount;

    @ApiModelProperty(value = "评价评论的次数")
    @TableField("comment_count")
    private Integer commentCount;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_date")
    private LocalDateTime createDate;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改时间")
    @TableField("update_date")
    private LocalDateTime updateDate;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;


}

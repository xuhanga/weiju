package com.huixi.microspur.web.controller;


import com.huixi.microspur.commons.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 诉求-评论 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjAppealComment")
public class WjAppealCommentController extends BaseController {

}


package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.WjChatRecord;

/**
 * <p>
 * 聊天室-聊天记录 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatRecordService extends IService<WjChatRecord> {

}

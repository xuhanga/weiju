package com.huixi.microspur.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 聊天室对应的用户
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_chat_user")
@ApiModel(value="WjChatUser对象", description="聊天室对应的用户")
public class WjChatUser implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "聊天室id")
    @TableField("chat_id")
    private String chatId;

    @ApiModelProperty(value = "聊天室对应的用户id（一对一，一对多）")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "创建时间（或者加入时间）")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "退出聊天的时间（一对多的聊天室可以退出群聊，需要个退出时间）")
    @TableField("quit_time")
    private LocalDateTime quitTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;


}

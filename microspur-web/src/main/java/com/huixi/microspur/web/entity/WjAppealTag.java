package com.huixi.microspur.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 诉求-对应标签
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal_tag")
@ApiModel(value="WjAppealTag对象", description="诉求-对应标签")
public class WjAppealTag implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "标签的名字（冗余）")
    @TableField("tag_name")
    private String tagName;

    @ApiModelProperty(value = "标签对应字典表的id")
    @TableField("dict_id")
    private String dictId;

    @ApiModelProperty(value = "标签的解释（冗余）")
    @TableField("explains")
    private String explains;

    @ApiModelProperty(value = "标签可能需要的地址值(冗余)")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;


}

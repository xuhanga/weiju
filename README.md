## 汇溪和他们的小伙伴们

> **汇溪**： 汇溪成海之意，象征汇聚多方力量。



感谢大家的支持😉。没想到有这么多的人愿意加入，真是受宠若惊呀😆。很多的东西都没有规划好，毕竟第一次（多多见谅）。

工作经验尚浅，许多东西不懂，劳烦各位了😜





## 补充事项

1.记得在IDE上下载阿里巴巴的编码检测	Alibaba Java Coding Guidelines，**力求规范**

2.小程序的Git地址：https://gitee.com/huixi_and_their_friends/weiju-wechat.git

3.小程序后台的Git地址：https://gitee.com/huixi_and_their_friends/weiju.git



## 项目结构简介

```
microspur
├── microspur-commons -- 通用的公共模块
├	└── com.huixi.microspur.commons
├		├── base.BaseContrller -- 所有模块都的controller都继承了它
├		├── config -- 公共配置
├		├── constant -- 公共常量
├		├── enums -- 公共错误码
├		├── exception -- 异常拦截统一包装
├		└── util （工具类用 hutool）
├			├── id -- 统一id生成器
├			├── validators -- 统一验证器
├			└── wrapper -- controller返回值包装类
├── microspur-generator -- 代码生成
├── microspur-web -- 小程序
├	└── com.huixi.microspur.web
├    	├── aspect -- 异常拦截器
├    	├── config -- 配置文件
├    	├── controller -- 接口
├    	├── entity -- 实体类
├    	├── mapper -- 方法接口
├  		└── service -- 方法实现
└── microspur-sysadmin -- 管理后台（结构同上）

项目分两个包部署。 一个应对小程序 另一个应对管理后台
```



## 项目注意事项

> 后端项目用的是： https://gitee.com/bweird/lenosp.git



1. 微信小程序的配置

   ```yaml
   在 application.yml 文件中（在IDEA 中双击 shift搜索）
   
   weiJu:
     appId:
     appSecret:
     
     填上你申请小程序的 appid 和 appSecret 
   ```

2. 数据库 （mysql）

   ```
   数据库ip: 47.106.225.52
   用户名： root
   密码： weiju
   
   搭建的一个测试库随便搞
   ```

3. 存储

   > 文件的存储用的是阿里的OSS 。 一系列配置都在文件中有。 我分派的是一个子账号，权限只能操作OSS，不用担心安全问题。

4. 项目切换环境

   > 切换生产或测试环境。 需要在pom 文件中设置。

